﻿using UnityEngine;
using System.Collections;

public class GetUserLocation : MonoBehaviour
{
    [HideInInspector] public float Latitude = -1;
    [HideInInspector] public float Longitude = -1;

    public IEnumerator LocationCoroutine()
    {
        // Starts the location service.
        Input.location.Start();

        // Waits until the location service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // If the service didn't initialize in 20 seconds this cancels location service use.
        if (maxWait < 1)
        {
            yield break;
        }

        // If the connection failed this cancels location service use.
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            yield break;
        }

        // If the connection succeeded, this retrieves the device's current location and displays it in the Console window.
        Latitude = Input.location.lastData.latitude;
        Longitude = Input.location.lastData.longitude;

        // Stops the location service if there is no need to query location updates continuously.
        Input.location.Stop();
    }
}