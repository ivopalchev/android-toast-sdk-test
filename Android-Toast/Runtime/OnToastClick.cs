using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class OnToastClick : MonoBehaviour
{
    public string toastMessage;
    private GetUserLocation locationManager;
    private WeatherManager weatherManager;

    void Start()
    {
        locationManager = GetComponent<GetUserLocation>();
        weatherManager = GetComponent<WeatherManager>();
    }
    
    public void ToastClick()
    {
        ToastManager.DisplayToast(toastMessage);
    }

    public void ToastClickWeather()
    {
        StartCoroutine(DisplayToastMessage());
    }

    IEnumerator DisplayToastMessage()
    {
        //First get location
        if (locationManager == null || weatherManager == null)
            yield break;
        
        yield return StartCoroutine(locationManager.LocationCoroutine());

        //Get Weather response
        float latitude = locationManager.Latitude;
        float longitude = locationManager.Longitude;
        
        yield return StartCoroutine(weatherManager.GetRequest(WeatherManager.WEATHER_API_ENDPOINT_FIRST_PART + latitude + "&" +
                                                              WeatherManager.WEATHER_API_ENDPOINT_MIDDLE_PART + longitude +
                                                              WeatherManager.WEATHER_API_ENDPOINT_FINAL_PART));

        //Display response to Toast
        ToastManager.DisplayToast("Max Temperature is: " + weatherManager.DeserializedWeatherResponse.daily.temperature_2m_max[0]
                                                         + " " + weatherManager.DeserializedWeatherResponse.daily_units.temperature_2m_max);
    }
}