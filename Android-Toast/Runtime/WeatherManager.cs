using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class WeatherManager : MonoBehaviour
{
    //"https://api.open-meteo.com/v1/forecast?latitude=19.07&longitude=72.87&timezone=IST&daily=temperature_2m_max";
    public static string WEATHER_API_ENDPOINT_FIRST_PART = "https://api.open-meteo.com/v1/forecast?latitude=";
    public static string WEATHER_API_ENDPOINT_MIDDLE_PART = "&longitude=";
    public static string WEATHER_API_ENDPOINT_FINAL_PART = "&timezone=IST&daily=temperature_2m_max";

    public Root DeserializedWeatherResponse;

    public IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);

                    DeserializedWeatherResponse = JsonConvert.DeserializeObject<Root>(webRequest.downloadHandler.text);

                    yield break;
            }
        }
    }
}

public class Daily
{
    public List<string> time { get; set; }
    public List<double> temperature_2m_max { get; set; }
}

public class DailyUnits
{
    public string time { get; set; }
    public string temperature_2m_max { get; set; }
}

public class Root
{
    public double latitude { get; set; }
    public double longitude { get; set; }
    public double generationtime_ms { get; set; }
    public int utc_offset_seconds { get; set; }
    public string timezone { get; set; }
    public string timezone_abbreviation { get; set; }
    public double elevation { get; set; }
    public DailyUnits daily_units { get; set; }
    public Daily daily { get; set; }
}