# CleverTap-Unity-SDK-Test

This simple project contains a simple scene with a canvas.

To get started and show a toast message for android first import the package (Android-Toast)
inside the folder and after that you can use the prefab inside the package (Runtime/Toast UI Button)
Place this prefab inside the canvas and in the OnToastClick referenced script you can type
 what messsage to be shown when its clicked on android !

To complete the second part of the assignment, Drag the Toast UI Button Weather Fetch prefab
into the canvas and when you click this button in android, it will fetch the latitude/longtide of the
device and after that pass those two parameters to a http request to the weather API and on complete
display a Toast Message to the user with the max temperature from the object that is recieved.

!!! Very Important !!! - Make sure location is turned on when testing the APK !!!